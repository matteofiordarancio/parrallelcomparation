package tryStream;

import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class BestWayToWorkWithAMatrix {
    private final static int SIZE=15000;
    private final static int[][] matrix = new int[SIZE][SIZE];

    public static void main(final String[] args) {
        final Timer temp = new Timer();
        //System.out.println(Arrays.deepToString(matrix));
        temp.start();
        useStream();
        temp.stop();
        //System.out.println(Arrays.deepToString(matrix));
        temp.start();
        useParrallelStream();
        temp.stop();
        //System.out.println(Arrays.deepToString(matrix));
        temp.start();
        useExecutor();
        temp.stop();
        //System.out.println(Arrays.deepToString(matrix));
        temp.start();
        classicMultiThreadMethod();
        temp.stop();
        //
        temp.start();
        noMultiThread();
        temp.stop();
        //System.out.println(Arrays.deepToString(matrix));
    }

    private static void noMultiThread() {
        performOperation(0, SIZE);
    }

    public static void useStream() {
        for(int i=0;i<SIZE;i++) {
            matrix[i] = Arrays.stream(matrix[i]).map(j->j=j+1).toArray();
        }
    }

    public static void useParrallelStream() {
        for(int i=0;i<SIZE;i++) {
            matrix[i] = Arrays.stream(matrix[i]).parallel().map(j->j=j+1).toArray();
        }
    }

    public static void useExecutor() {
        final int threadPoolSize = Runtime.getRuntime().availableProcessors()+1;
        final ExecutorService exec = Executors.newFixedThreadPool(threadPoolSize);

        for(int i=0;i<SIZE;i++) {
            final int index=i;
            exec.execute(()->{
                for(int j=0;j<SIZE;j++) {
                    matrix[index][j]+=1;
                }
            });
        }
        exec.shutdown();
        try {
            exec.awaitTermination(10, TimeUnit.SECONDS);
        } catch (final InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public static void classicMultiThreadMethod() {
        final int threadPoolSize = Runtime.getRuntime().availableProcessors()+1;
        //5 nel mio sistema
        final Thread[] arrThread = new Thread[threadPoolSize];
        final int section = SIZE / threadPoolSize;

        for(int i=0;i<threadPoolSize;i++) {
            final int parte=i;
            arrThread[i]=new Thread(()-> {
                performOperation(section*parte, section*(parte+1));
            });
            arrThread[i].start();
        }
        for (final Thread thread : arrThread) {
            try {
                thread.join();
            } catch (final InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        performOperation(section*threadPoolSize, SIZE);
    }

    public static void performOperation(final int begin, final int end) {
        for(int i=begin;i<end;i++) {
            for(int j=0;j<SIZE;j++) {
                matrix[i][j]+=1;
            }
        }
    }
}
