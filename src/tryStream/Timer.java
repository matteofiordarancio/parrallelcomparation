package tryStream;

public class Timer {

    long startTime;
    public void start() {
        startTime = System.currentTimeMillis();
    }
    public void stop() {
        System.out.println("Time passed in milliseconds: " + (System.currentTimeMillis()-startTime));
    }
}
